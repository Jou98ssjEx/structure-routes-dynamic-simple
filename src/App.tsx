import { AppRouters } from "./routers"

export const App = () => {
  return (
      <AppRouters />
  )
}
